import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main_page extends Pane {
    public JPanel field = this;
    private int mandat;
    private String login;
    private String password;

    public Main_page(int m, String login, String password) {
        super();
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));

        if (m == 0) {
            panel.add(quotes_button());
        } else if (m == 1) {
            panel.add(quotes_button());
            panel.add(settings_button());
            panel.add(change_button());
        } else if (m == 2) {
            panel.add(quotes_button());
            panel.add(users_button());
            panel.add(settings_button());
            panel.add(change_button());
        } else {
            panel.add(quotes_button());
            panel.add(users_button());
            panel.add(settings_button());
            panel.add(change_button());
        }
        add(panel);
        add(new JLabel());
        mandat = m;
        this.login = login;
        this.password = password;
    }

    private JButton quotes_button() {
        JButton quotes = new JButton("Цитаты");
        MyButtonListenerQuotes m = new MyButtonListenerQuotes();
        quotes.addActionListener(m);
        return quotes;
    }

    private JButton users_button() {
        JButton users = new JButton("Пользователи");
        MyButtonListenerUsers m1 = new MyButtonListenerUsers();
        users.addActionListener(m1);
        return users;
    }

    private JButton settings_button() {
        JButton settings = new JButton("Настройки");
        MyButtonListenerSettings m2 = new MyButtonListenerSettings();
        settings.addActionListener(m2);
        return settings;
    }

    private JButton change_button() {
        JButton change_button = new JButton("Редактирование цитат");
        MyButtonListenerChange m = new MyButtonListenerChange();
        change_button.addActionListener(m);
        return change_button;
    }

    public JPanel quotes() {
        JPanel panel = new JPanel();
        try {
            Table_quotes quotes = new Table_quotes();
            JPanel panel2 = new JPanel();
            panel2.setLayout(new GridLayout(quotes.size(), 1));
            for (int i = 0; i < quotes.size(); i++) {
                JPanel panel1 = new JPanel();
                panel1.setLayout(new GridLayout(2, 2));
                JLabel quote = new JLabel(quotes.get(i).getQuote());
                JLabel teacher = new JLabel(quotes.get(i).getTeacher());
                JLabel subject = new JLabel(quotes.get(i).getSubject());
                JLabel date = new JLabel(quotes.get(i).getDate());
                panel1.add(quote);
                panel1.add(subject);
                panel1.add(teacher);
                panel1.add(date);
                panel2.add(panel1);
            }
            JScrollPane scrollPane = new JScrollPane(panel2);
            panel.add(scrollPane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return panel;
    }

    public JPanel changeQuote() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        Requests request = new Requests();
        JPanel panel2 = new JPanel();
        if (mandat == 1) {
            try {
                panel2.setLayout(new GridLayout(request.countQuotes(request.userByLogin(login)), 1));
                ResultSet resultSet = request.quotesOfUser(request.userByLogin(login));
                for (int i = 0; i < request.countQuotes(request.userByLogin(login)); i++) {
                    resultSet.next();
                    JPanel panel3 = new JPanel();
                    panel3.setLayout(new GridLayout(2, 2));
                    JTextField quote = new JTextField(resultSet.getString(1));
                    JButton delete = new JButton("Удалить цитату");
                    JButton change = new JButton("Редактировать цитату");
                    panel3.add(quote);
                    panel3.add(delete);
                    panel3.add(change);
                    panel2.add(panel3);
                    delete.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Requests request = new Requests();
                            try {
                                request.deleteQuote(resultSet.getInt(2));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    change.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Requests request = new Requests();
                            try {
                                request.changeQuote(quote.getText(), resultSet.getInt(2));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (mandat == 2) {
            try {
                panel2.setLayout(new GridLayout(request.countQuotesByGroup(request.groupByLogin(login)), 1));
                ResultSet resultSet = request.showQuotesOfGroup(request.groupByLogin(login));
                for (int i = 0; i < request.countQuotesByGroup(request.groupByLogin(login)); i++) {
                    resultSet.next();
                        JPanel panel3 = new JPanel();
                        panel3.setLayout(new GridLayout(2, 2));
                        JTextField quote = new JTextField(resultSet.getString(1));
                        JButton delete = new JButton("Удалить цитату");
                        JButton change = new JButton("Редактировать цитату");
                        panel3.add(quote);
                        panel3.add(delete);
                        panel3.add(change);
                        delete.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Requests request = new Requests();
                                try {
                                    request.deleteQuote(resultSet.getInt(2));
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        change.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Requests request = new Requests();
                                try {
                                    request.changeQuote(quote.getText(), resultSet.getInt(2));
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        panel2.add(panel3);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                panel2.setLayout(new GridLayout(request.countAllQuotes(), 1));
                ResultSet resultSet = request.showQuotes();
                for (int i = 0; i < request.countAllQuotes(); i++) {
                    resultSet.next();
                    JPanel panel3 = new JPanel();
                    panel3.setLayout(new GridLayout(2, 2));
                    JTextField quote = new JTextField(resultSet.getString(1));
                    JButton delete = new JButton("Удалить цитату");
                    JButton change = new JButton("Редактировать цитату");
                    panel3.add(quote);
                    panel3.add(delete);
                    panel3.add(change);
                    panel2.add(panel3);
                    delete.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Requests request = new Requests();
                            try {
                                request.deleteQuote(resultSet.getInt(2));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    change.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Requests request = new Requests();
                            try {
                                request.changeQuote(quote.getText(), resultSet.getInt(2));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    System.out.println(resultSet.getInt(2) + resultSet.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        JScrollPane scrollPane = new JScrollPane(panel2);
        panel.add(scrollPane);
        JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayout(4, 1));
        JButton add = new JButton("Добавить цитату");
        JTextField new_quote = new JTextField("Введите цитату");
        JTextField new_teacher = new JTextField("Введите ФИО преподавателя");
        JTextField new_subject = new JTextField("Введите название предмета");
        JTextField new_date = new JTextField("Введите дату");
        panel3.add(new_quote);
        panel3.add(new_teacher);
        panel3.add(new_subject);
        panel3.add(new_date);
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Requests request = new Requests();
                try {
                    request.addQuote(new_quote.getText(), new_teacher.getText(), new_subject.getText(), new_date.getText(), request.userByLogin(login));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
        panel3.add(add);
        panel.add(panel3);
        return panel;
    }

    public JPanel users() {
        JPanel panel = new JPanel();
        try {
            JPanel panel2 = new JPanel();
            if(mandat == 3) {
                Table_users users = new Table_users();
                panel2.setLayout(new GridLayout(users.size(), 1));
                for (int i = 0; i < users.size(); i++) {
                    JPanel panel1 = new JPanel();
                    panel1.setLayout(new GridLayout(1, 2));
                    JButton delete = new JButton("Удалить пользователя");
                    JLabel login = new JLabel(users.get(i).getLogin());
                    panel1.add(login);
                    panel1.add(delete);
                    panel2.add(panel1);
                    delete.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Requests request = new Requests();
                            try {
                                request.deleteUser(request.userByLogin(login.getText()));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                }
            }
            else {
                Requests request = new Requests();
                panel2.setLayout(new GridLayout(request.countUsersByGroup(request.groupByLogin(login)), 1));
                ResultSet res = request.loginByGroup(request.groupByLogin(login));
                for (int i = 0; i < request.countUsersByGroup(request.groupByLogin(login)); i++) {
                    res.next();
                    JPanel panel1 = new JPanel();
                    panel1.setLayout(new GridLayout(1, 2));
                    JButton delete = new JButton("Удалить пользователя");
                    JLabel login = new JLabel(res.getString(1));
                    panel1.add(login);
                    panel1.add(delete);
                    panel2.add(panel1);
                    delete.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Requests request = new Requests();
                            try {
                                request.deleteUser(request.userByLogin(login.getText()));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                }
            }
            JScrollPane scrollPane = new JScrollPane(panel2);
            panel.add(scrollPane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return panel;
    }

    public JPanel settings() {
        Requests request = new Requests();
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));
        JTextField log = new JTextField(login);
        JTextField pass = new JTextField(password);
        JButton change = new JButton("Изменить");
        JLabel count = new JLabel();
        try {
            count = new JLabel("Количество ваших цитат: " + request.countQuotes(request.userByLogin(login)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        change.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Requests request = new Requests();
                try {
                    request.changePassword(pass.getText(), request.userByLogin(login));
                    request.changeLogin(log.getText(), request.userByLogin(login));

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
        panel.add(log);
        panel.add(pass);
        panel.add(change);
        panel.add(count);
        return panel;
    }

    private class MyButtonListenerQuotes implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            field.remove(1);
            field.add(quotes());
            field.revalidate();
            field.repaint();
        }
    }

    private class MyButtonListenerUsers implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            field.remove(1);
            field.add(users());
            field.revalidate();
            field.repaint();
        }
    }

    private class MyButtonListenerSettings implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            field.remove(1);
            field.add(settings());
            field.revalidate();
            field.repaint();
        }
    }

    private class MyButtonListenerChange implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            field.remove(1);
            field.add(changeQuote());
            field.revalidate();
            field.repaint();
        }
    }
}
