import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Start extends Pane{
    public JPanel field = this;
    private JComboBox mandat;
    private JTextField login;
    private JTextField password;
    private JComboBox group_num;

    public Start() {
        super();
        add(buttons());
        add(new JLabel());
    }

    private JPanel buttons() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 1));
        panel.setPreferredSize((new Dimension(10, 500)));
        JButton registr = new JButton("Зарегистрироваться");
        JButton login = new JButton("Войти");
        JButton guest = new JButton("Войти гостем");
        panel.add(registr);
        panel.add(login);
        panel.add(guest);
        MyButtonListenerRegistr m = new MyButtonListenerRegistr();
        MyButtonListenerLogin m1 = new MyButtonListenerLogin();
        MyButtonListenerGuest m2 = new MyButtonListenerGuest();
        registr.addActionListener(m);
        login.addActionListener(m1);
        guest.addActionListener(m2);
        return panel;
    }

    private JPanel reg() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(5, 1));
        login = new JTextField("Введите логин");
        password = new JTextField("Введите пароль");
        String[] items = {
                "Пользователь",
                "Верификатор"
        };
        String[] groups = {
                "1",
                "2",
                "3"
        };
        mandat = new JComboBox(items);
        group_num = new JComboBox(groups);
        JButton reg = new JButton("Зарегистрироваться");
        panel.add(login);
        panel.add(password);
        panel.add(mandat);
        panel.add(group_num);
        panel.add(reg);
        MyButtonListenerReg m = new MyButtonListenerReg();
        reg.addActionListener(m);
        return panel;
    }

    private JPanel login() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 1));
        login = new JTextField("Введите логин");
        password = new JTextField("Введите пароль");
        JButton go = new JButton("Войти");
        panel.add(login);
        panel.add(password);
        panel.add(go);
        MyButtonListenerLog m = new MyButtonListenerLog();
        go.addActionListener(m);
        return panel;
    }

    public void change(int mandat, String login, String password) {
        JPanel pane = Main.getPane();
        pane.remove(0);
        Main_page page = new Main_page(mandat, login, password);
        pane.add(page);
        pane.revalidate();
        pane.repaint();
    }

    private class MyButtonListenerRegistr implements ActionListener {
        public void actionPerformed(ActionEvent e){
            field.remove(1);
            field.add(reg());
            field.revalidate();
            field.repaint();
        }
    }

    private class MyButtonListenerLogin implements ActionListener {
        public void actionPerformed(ActionEvent e){
            field.remove(1);
            field.add(login());
            field.revalidate();
            field.repaint();
        }
    }

    private class MyButtonListenerGuest implements ActionListener {
        public void actionPerformed(ActionEvent e){
            change(0, null, null);
        }
    }

    private class MyButtonListenerReg implements ActionListener {
        public void actionPerformed(ActionEvent e){
            Requests request = new Requests();
            try {
                request.addUser(login.getText(), password.getText(), (String) group_num.getSelectedItem());
                if(mandat.getSelectedItem().equals("Пользователь")) {
                    request.addMandat(login.getText(), 1);
                    change(1, login.getText(), password.getText());
                }
                else {
                    request.addMandat(login.getText(), 2);
                    change(2, login.getText(), password.getText());
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class MyButtonListenerLog implements ActionListener {
        public void actionPerformed(ActionEvent e){
            Requests request = new Requests();
            try {
                if(request.truePassword(login.getText(), password.getText())) {
                    change(request.mandatByLogin(login.getText()), login.getText(), password.getText());
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
