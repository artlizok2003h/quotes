import java.sql.*;

public class Requests {
    private Connection connection;

    public Requests() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
//
            connection = DriverManager.getConnection(
                    "jdbc:mysql://std-mysql.ist.mospolytech.ru:3306/std_2001_quotes",
                    "std_2001_quotes", "12345678");
//
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void addUser(String login, String password, String group_num) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO users VALUES (?, ?, ?, ?)");
        statement.setNull(1, 1);
        statement.setString(2, login);
        statement.setString(3, String.valueOf(password.hashCode()));
        statement.setString(4, group_num);
        statement.executeUpdate();
    }

    public void addMandat(String login, int mandat) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO mandat VALUES (?, ?, ?)");
        statement.setNull(1, 1);
        statement.setInt( 2, userByLogin(login));
        statement.setInt(3, mandat);
        statement.executeUpdate();
    }

    public int userByLogin(String login) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT id FROM `users` WHERE login = (?);");
        statement.setString(1, login);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    public int mandatByLogin(String login) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT mandat FROM `mandat` WHERE id_user = (?);");
        statement.setInt(1, userByLogin(login));
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    public boolean truePassword(String login, String password) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT password_hash FROM users WHERE login = (?);");
        statement.setString(1, login);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return String.valueOf(password.hashCode()).equals(resultSet.getString(1));
    }

    public void deleteUser(int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id = (?);");
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    public void changeLogin(String new_login, int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("UPDATE users SET login = (?) WHERE id = (?);");
        statement.setString(1, new_login);
        statement.setInt(2, id);
        statement.executeUpdate();
    }

    public void changePassword(String new_password, int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("UPDATE users SET password_hash = (?) WHERE id = (?);");
        statement.setInt(1, new_password.hashCode());
        statement.setInt(2, id);
        statement.executeUpdate();
    }

    public void addQuote(String quote, String teacher, String subject, String date, int id_user) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO quotes VALUES(?, ?, ?, ?, ?, ?);");
        statement.setNull(1, 1);
        statement.setString(2, quote);
        statement.setString(3, teacher);
        statement.setString(4, subject);
        statement.setString(5, date);
        statement.setInt(6, id_user);
        statement.executeUpdate();
    }

    public void deleteQuote(int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("DELETE FROM quotes WHERE id = (?);");
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    public void changeQuote(String new_quote, int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("UPDATE quotes SET quote = (?) WHERE id = (?);");
        statement.setString(1, new_quote);
        statement.setInt(2, id);
        statement.executeUpdate();
    }

    public ResultSet showQuotes() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT quote, id FROM quotes;");
        return statement.executeQuery();
    }

    public int countQuotes(int id_user) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT COUNT(quote) FROM quotes WHERE id_user = (?);");
        statement.setInt(1, id_user);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        return count;
    }

    public int countAllQuotes() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT COUNT(quote) FROM quotes;");
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        return count;
    }

    public ResultSet quotesOfUser(int id_user) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT quote, id FROM quotes WHERE id_user = (?);");
        statement.setInt(1, id_user);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public ResultSet getUsers() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM users;");
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public ResultSet getQuotes() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM quotes;");
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public String groupByLogin(String login) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT group_num FROM `users` WHERE login = (?);");
        statement.setString(1, login);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getString(1);
    }

    public int countQuotesByGroup(String group_num) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT COUNT(quote) FROM quotes JOIN users WHERE id_user = users.id AND group_num = (?);");
        statement.setString(1, group_num);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        return count;
    }

    public ResultSet showQuotesOfGroup(String group_num) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT quote, quotes.id FROM quotes JOIN users WHERE id_user = users.id AND group_num = (?);");
        statement.setString(1, group_num);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public int countUsersByGroup(String group_num) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT COUNT(id) FROM users WHERE group_num = (?);");
        statement.setString(1, group_num);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        return count;
    }

    public ResultSet loginByGroup(String group_num) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT login FROM users WHERE group_num = (?);");
        statement.setString(1, group_num);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }
}





