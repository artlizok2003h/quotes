import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Table_users extends ArrayList<Table_entry_users> {
    public Table_users() throws SQLException {
        Requests request = new Requests();
        ResultSet resultSet = request.getUsers();
        while (resultSet.next()) {
            this.add(new Table_entry_users(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
        }
    }
}

