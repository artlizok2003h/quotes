public class Table_entry_mandats {
    private int id;
    private int id_user;
    private int mandat;

    public Table_entry_mandats(int id, int id_user, int mandat) {
        this.id = id;
        this.id_user = id_user;
        this.mandat = mandat;
    }

    public int getId() {
        return id;
    }

    public int getId_user() {
        return id_user;
    }

    public int getMandat() {
        return mandat;
    }
}
