import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Table_quotes extends ArrayList<Table_entry_quotes> {
    public Table_quotes() throws SQLException {
        Requests request = new Requests();
        ResultSet resultSet = request.getQuotes();
        while (resultSet.next()) {
            this.add(new Table_entry_quotes(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6)));

        }
    }
}