import javax.swing.*;


public class Main extends JFrame{
    private static JPanel pane;

    public Main(){
        this.setTitle("Quotes");
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        pane = (JPanel) this.getContentPane();
        Start start = new Start();
        pane.add(start);

        this.setVisible(true);

    }
    public static void main(String[] args) {
        new Main();
    }

    public static JPanel getPane() {
        return pane;
    }
}
