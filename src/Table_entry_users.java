public class Table_entry_users {
    private int id;
    private String login;
    private String password_hash;

    public Table_entry_users(int id, String login, String password_hash) {
        this.id = id;
        this.login = login;
        this.password_hash = password_hash;
    }

    public String getLogin() {
        return login;
    }
}
