public class Table_entry_quotes {
    private int id;
    private String quote;
    private String teacher;
    private String subject;
    private String date;
    private int id_user;

    public Table_entry_quotes(int id, String quote, String teacher, String subject, String date, int id_user) {
        this.id = id;
        this.quote = quote;
        this.teacher = teacher;
        this.subject = subject;
        this.date = date;
        this.id_user = id_user;
    }

    public String getQuote() {
        return quote;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getSubject() {
        return subject;
    }

    public String getDate() {
        return date;
    }
}
